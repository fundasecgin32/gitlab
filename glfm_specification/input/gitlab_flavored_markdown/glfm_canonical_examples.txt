# GitLab-Specific Markdown

Currently, only some of the GitLab-specific markdown features are
listed in this section. We will eventually add all
GitLab-specific features currently listed as supported in the
[user-facing documentation for GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html).

There is currently only this single top-level heading, but the
examples may be split into multiple top-level headings in the future.

## Footnotes

See
[the footnotes section of the user-facing documentation for GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html#footnotes).

```````````````````````````````` example gitlab footnote
footnote reference tag [^fortytwo]

[^fortytwo]: footnote text
.
<p>
footnote reference tag
<sup>
<a href="#fn-fortytwo-42" id="fnref-fortytwo-42" data-footnote-ref>
1
</a>
</sup>
</p>
<section data-footnotes>
<ol>
<li id="fn-fortytwo-42">
<p>
footnote text
<a href="#fnref-fortytwo-42" data-footnote-backref>
</a>
</p>
</li>
</ol>
</section>
````````````````````````````````
